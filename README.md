# tap-netsuite

This is a [Singer](https://singer.io) tap that extracts data from a NetSuite Account and produces JSON-formatted data following the [Singer spec](https://github.com/singer-io/getting-started/blob/master/docs/SPEC.md). 

At the core of tap-netsuite is a SOAP client that connects with NetSuite's [SuiteTalk Web Services](http://www.netsuite.com/portal/developers/resources/suitetalk-documentation.shtml), manages the API calls and fetches the data requested by the other processes of the tap. 

In the current version of tap-netsuite, all types of supported Transactions are fetched, together with the referenced expenses, transaction items, applications and transaction lines. We also fetch the most important support entities: currencies, departments, subsidiaries and accounts.